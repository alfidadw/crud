@extends('layouts.master')

@section('title')
Tambah Cast
@endsection

@section('content')
<div>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for ="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for ="title">Umur</label>
                <input type="text" class="form-control" name="umur" id="body" placeholder="Masukan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for ="title">Biodata</label>
                <input type="text" class="form-control" name="bio" id="body" placeholder="Masukan Biodata">
                @error('bio')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
@endsection