@extends('layouts.master')

@section('title')
Edit Cast {{$casts->id}}
@endsection

@section('content')
<div> 
        <form action="/cast/{{$casts->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$casts->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Umur</label>
                <input type="text" class="form-control" name="umur" value="{{$casts->umur}}" id="umur" placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Biodata</label>
                <input type="text" class="form-control" name="bio" value="{{$casts->bio}}" id="bio" placeholder="Masukkan Biodata">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection